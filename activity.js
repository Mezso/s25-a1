db.fruits.insertMany([
		{
			name : "Banana",
			supplier : "Farmer fruits Inc.",
			stocks : 30,
			price : 20,
			onSale: true
		},
		{
			name : "Mango",
			supplier : "Mango Magic Inc.",
			stocks : 50,
			price : 70,
			onSale: true
		},
		{
			name : "Dragon Fruit",
			supplier : "Farmer fruits Inc.",
			stocks : 10,
			price : 60,
			onSale: true
		},
		{
			name : "Grapes",
			supplier : "Fruits Co.",
			stocks : 30,
			price : 100,
			onSale: true
		},
		{
			name : "Apple",
			supplier : "Apple Valley",
			stocks : 0,
			price : 20,
			onSale: false
		},
		{
			name : "Papaya",
			supplier : "Fruits Co.",
			stocks : 15,
			price : 60,
			onSale: true
		}

	])
// number 2
db.fruits.aggregate([
		{
			$match: {onSale:true}
		},
		{
			$group: {_id:null,count:{$sum:1}}
		}
	])
// number 3
db.fruits.aggregate([
		{
			$match: {stocks:{$gt : 20}}
		},
		{
			$group: {_id:null,count:{$sum:1}}
		}
	])
// number 4
db.fruits.aggregate([
		{
			$match: {onSale:true}
		},
		{
			$group: {_id:"$supplier",count:{$avg:"$price"}}
		}
	])
// number 5
db.fruits.aggregate([
		{
			$group: {_id:"$supplier",count:{$max:"$price"}}
		}
	])
db.fruits.aggregate([
		{
			$group: {_id:"$supplier",count:{$min:"$price"}}
		}
	])